package com.real.estate.service.services.http;


import com.real.estate.service.utils.HttpErrorException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class Response {

    public static <T> ResponseEntity<SystemResponse<T>> internalServerError(String msg) {
        return ResponseEntity
                .status(500)
                .body(new SystemResponse<>(500, msg));
    }

    public static <T> ResponseEntity<SystemResponse<T>> httpError(HttpErrorException e) {
        return ResponseEntity
                .status(e.getStatus())
                .body(new SystemResponse<>(500, e.getMessage()));
    }

    public static <T> ResponseEntity<SystemResponse<T>> badRequest(String msg) {
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(new SystemResponse<>(400, msg));
    }

    public static <T> ResponseEntity<SystemResponse<T>> badRequest(int code, String msg) {
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(new SystemResponse<>(code, msg));
    }

    public static <T> ResponseEntity<SystemResponse<T>> badRequest(int code, String msg, T data) {
        return ResponseEntity
                .badRequest()
                .body(new SystemResponse<>(code, msg, data));
    }

    public static <T> ResponseEntity<SystemResponse<T>> failDependency(int code, String msg) {
        return ResponseEntity
                .status(424)
                .body(new SystemResponse<>(code, msg));
    }

    public static <T> ResponseEntity<SystemResponse<T>> ok(int code, String msg, T body) {
        return ResponseEntity.ok(new SystemResponse<>(code, msg, body));
    }

    public static <T> ResponseEntity<SystemResponse<T>> ok(int code, String msg) {
        return ResponseEntity.ok(new SystemResponse<>(code, msg));
    }

}
