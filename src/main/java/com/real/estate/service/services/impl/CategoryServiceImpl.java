package com.real.estate.service.services.impl;

import com.real.estate.service.model.out.CategoryResponse;
import com.real.estate.service.repositories.CategoryRepository;
import com.real.estate.service.repositories.entities.CategoryEntity;
import com.real.estate.service.services.ICategoryService;
import com.real.estate.service.services.http.Response;
import com.real.estate.service.services.http.SystemResponse;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CategoryServiceImpl implements ICategoryService {

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private CategoryRepository categoryRepository;

    @Override
    public ResponseEntity<SystemResponse<List<CategoryResponse>>> getAll() {

        List<CategoryEntity> list = categoryRepository.findAll();

        List<CategoryResponse> result = new ArrayList<>();

        for (CategoryEntity c:list
             ) {
            CategoryResponse categoryResponse = modelMapper.map(c,CategoryResponse.class);
            result.add(categoryResponse);
        }

        return Response.ok(200,"success",result);
    }
}
