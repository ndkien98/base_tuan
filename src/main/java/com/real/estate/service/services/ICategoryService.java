package com.real.estate.service.services;

import com.real.estate.service.model.out.CategoryResponse;
import com.real.estate.service.services.http.SystemResponse;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface ICategoryService {

    ResponseEntity<SystemResponse<List<CategoryResponse>>> getAll();
}
