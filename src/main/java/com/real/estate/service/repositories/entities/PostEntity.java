package com.real.estate.service.repositories.entities;

import lombok.Data;

import javax.persistence.*;

@Entity(name = "post")
@Data
public class PostEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int id;

    @Column(name = "name")
    private String name;

    @Column(name = "short_desciption")
    private String shortDescription;

    @Column(name = "avatar")
    private String avartar;

    @Column(name = "status")
    private int status;

}
