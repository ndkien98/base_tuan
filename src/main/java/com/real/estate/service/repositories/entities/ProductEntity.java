package com.real.estate.service.repositories.entities;

import lombok.Data;

import javax.persistence.*;

@Entity(name = "production")
@Data
public class ProductEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name = "acreage")
    private float acreage;

    @Column(name = "price")
    private long price;

    @Column(name = "description")
    private String description;

    @Column(name = "id_category")
    private int idCategory;

    @Column(name = "status")
    private int status;

}
