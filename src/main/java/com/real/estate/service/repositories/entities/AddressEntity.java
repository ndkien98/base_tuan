package com.real.estate.service.repositories.entities;

import lombok.Data;

import javax.persistence.*;

@Entity(name = "address")
@Data
public class AddressEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "city")
    private String city;

    @Column(name = "distric")
    private String distric;

    @Column(name = "commune")
    private String commune;

    @Column(name = "detail")
    private String detail;

}
