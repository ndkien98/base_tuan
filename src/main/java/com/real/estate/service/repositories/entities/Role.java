package com.real.estate.service.repositories.entities;

import lombok.Data;

import javax.persistence.*;

@Entity(name = "role")
@Data
public class Role {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "name")
    public String name;
}
