package com.real.estate.service.repositories.entities;

import lombok.Data;

import javax.persistence.*;

@Entity(name = "category")
@Data
public class CategoryEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int id;

    @Column(name = "name")
    private String name;

    @Column(name = "descreption")
    private String description;

}
