package com.real.estate.service.controllers;

import com.real.estate.service.model.out.CategoryResponse;
import com.real.estate.service.services.ICategoryService;
import com.real.estate.service.services.http.SystemResponse;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/category")
public class CategoryController {

    @Autowired
    private ICategoryService categoryService;

    @RequestMapping("get-all")
    public ResponseEntity<SystemResponse<List<CategoryResponse>>> getAll(){
        return categoryService.getAll();
    }

}
