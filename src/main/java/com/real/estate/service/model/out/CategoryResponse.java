package com.real.estate.service.model.out;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Data
public class CategoryResponse {

    public int id;

    private String name;

    private String description;

}
